﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnLimit : MonoBehaviour
{
    public float rate;
    public GameObject[] asteroids;
    void start()
    {
        InvokeRepeating("spawnAsteroid", rate, rate);
    }
    void spawnAsteroid()
    {
        Instantiate(asteroids[(int)Random.Range(0, asteroids.Length)], new Vector3(Random.Range(-10f, 10f), 4f, 0), Quaternion.identity);
    }
}