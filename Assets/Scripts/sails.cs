﻿using UnityEngine;
using System.Collections;

public class sails : MonoBehaviour
{

    public Transform target;//set target from inspector instead of looking in Update
    public float speed = 3f;
    // This allows the designer to modify the health of the sprite.
    public float health;


    void Update()
    {

        //rotate to look at the player
        transform.LookAt(target.position);
        transform.Rotate(new Vector3(0, -90, 0), Space.Self);//correcting the original rotation


        //move towards the player
        if (Vector3.Distance(transform.position, target.position) > 1f)
        {//move if distance from target is greater than 1
            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }
        void OnCollisionEnter2D(Collision2D col)
        {
            if (col.gameObject.tag == "Player")
            {
                col.gameObject.GetComponent<treads>().Damage();
                Die();
                Destroy(col.gameObject);
            }
            else if (col.gameObject.tag == "enemy")
            {
                Destroy(col.gameObject);
            }
        }
        void Die()
        {
            Destroy(gameObject);
        }
        void Damage()
        {
            health--;
            if (health == 0)
            {
                Die();
            }
        }
    }

}