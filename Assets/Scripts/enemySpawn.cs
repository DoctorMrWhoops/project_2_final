﻿using UnityEngine;
using System.Collections;

public class enemySpawn : MonoBehaviour
{
    // Attaching the game object of the enemy prefab.
    public GameObject enemyPrefab;
    // Variable in charge of declaring the maximum distance that the enemies can spawn.
    float spawnDistance = 5.25f;
    // Variable for the rate in which enemies spawn.
    public float enemyRate = 3;
    //  Variavble for the nextEnemy.
    public float nextEnemy = 1;

    // Update is called once per frame
    void Update()
    {
        // nextEnemy is now equal to the result of it being subtracted by Time.deltaTime.
        nextEnemy -= Time.deltaTime;
        // An if statement for when the variable nextEnemy is less than or equal to zero.
        if (nextEnemy <= 0)
        {
            // nextEnemy is equal to the value of enemyRate.
            nextEnemy = enemyRate;
            //enemyRate is equal to the enemyRate muliplied by 0.9.
            enemyRate *= 0.9f;
            // An if statement for when the enemyRate is less than 2.
            if (enemyRate < 2)
                enemyRate = 2;
            // Vector variable for offset is equal to the random value on a unit circle.
            Vector3 offset = Random.onUnitSphere;
            // offset.z is equal to zero (THIS IS DUE TO GAME BEING IN 2D).
            offset.z = 0;
            // offset is equal to the offset.normalized multiplied by the spawn distance.
            offset = offset.normalized * spawnDistance;
            // Make a duplicate of the enemy prefab and it's functionality.
            Instantiate(enemyPrefab, transform.position + offset, Quaternion.identity);
        }
    }
}