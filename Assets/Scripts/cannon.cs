﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cannon : MonoBehaviour
{
    public Transform firePoint;
    public GameObject laser;
    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Jump"))
        {
            Shoot();
        }
    }
    void Shoot()
    {
        Instantiate(laser, firePoint.position, firePoint.rotation);
    }
}
