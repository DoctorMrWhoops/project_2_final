﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class treads : MonoBehaviour
{
    //declares the rigid body commponent
    Rigidbody2D rb;
    //variable for speed
    public float speed;
    public float health;
    public float turnSpeed;

    public Transform tf;
    // Awake is called before the first frame update
    void Awake()
    {
        //calls the rigidbody
        rb = GetComponent<Rigidbody2D>();
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("up"))
        {
            tf.Translate(tf.up * speed * Time.deltaTime, Space.World);
        }
        // This line of code sets the "a" button to rotate the sprite left. 
        if (Input.GetKey("left"))
        {
            transform.Rotate(Vector3.forward * turnSpeed);
        }
        // This line of code sets the "d" button to rotate the sprite right.
        if (Input.GetKey("right"))
        {
            transform.Rotate(Vector3.back * turnSpeed);
        }
        //when force is applied object will go vertical

    }
    public void Damage()
    {
        health--;
        if(health == 0)
        {
            Destroy(gameObject);
        }
    }
}
